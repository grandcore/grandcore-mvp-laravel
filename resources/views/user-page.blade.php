<% var data = {
  title: "GrandCore | Страница",
  author: "Harrix"
}; %>
<%= _.template(require('./../includes/header.html').default)(data) %>

<div class="page-user">
  <div class="page-user-container">
    <!-- profile block -->
    <div class="page-user-profile page-user-profile_bottom">
      <!-- photo -->
      <div class="page-user-profile__img"></div>
      <!-- info -->
      <div class="page-user-profile__user-wrap">
        <!-- skills and name -->
        <h2 class="page-user-profile__title">
          Богдан Соколов
        </h2>
        <p class="page-user-profile__skills">
          PHP, JavaScript, Docker, Ruby, Figma, Photoshop
          Docker, Ruby, Figma, Photoshop, Laravel
        </p>
        <!-- status -->
        <div class="page-user-status">
          <a href="#" class="page-user-status__rank page-user-status_bottom">
            <img src="../../img/page-user/icon.png" alt="" class="page-user-status__rank-icon">
            Основатель
          </a>
          <p class="page-user-status__rating page-user-status_bottom">
            Рейтинг: 223
          </p>
        </div>
        <!-- btn -->
        <div class="page-user-btn">
          <button class="page-user-btn__edit">
            Edit Profile
          </button>
          <a href="#" class="page-user-btn__message">
            <img src="..//../img/page-user/telegram.svg" alt="" class="page-user-btn__message_telegram">
            Send a message
          </a>
        </div>
      </div>
    </div>
    <!-- Projects  -->
    <div class="page-user-projects ">
      <!-- menu -->
      <ul class="page-user-projects-menu">
        <!-- Projects Founded -->
        <li class="page-user-projects-menu__item">
          <a href="#" class="page-user-projects-menu__link page-user-projects-menu__link_active">
            Projects Founded 2
          </a>
        </li>
        <!-- Projects Supported -->
        <li class="page-user-projects-menu__item">
          <a href="#" class="page-user-projects-menu__link">
            Projects Supported 23
          </a>
        </li>
        <!-- Projects Liked -->
        <li class="page-user-projects-menu__item">
          <a href="#" class="page-user-projects-menu__link">
            Projects Liked 608
          </a>
        </li>
      </ul>
      <!-- projects cards -->
      <div class="page-user-cards">
        <!-- card -->
        <article class="page-user-card">
          <img src="../../img/page-user/page-user-bg1.jpg" alt="" class="page-user-card__img">
          <div class="page-user-card__content-wrap">
            <h3 class="page-user-card__title">
              Мы хотим вывести идеи Open Source на качественно
            </h3>
            <p class="page-user-card__text">
              Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн платформу для
              работы над любыми открытыми проектами...
            </p>
            <a href="#" class="page-user-card__link">
              Смотреть проект
            </a>
          </div>
        </article>
        <!-- card -->
        <article class="page-user-card">
          <img src="../../img/page-user/page-user-bg2.jpg" alt="" class="page-user-card__img">
          <div class="page-user-card__content-wrap">
            <h3 class="page-user-card__title">
              Мы хотим вывести идеи Open Source на качественно
            </h3>
            <p class="page-user-card__text">
              Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн платформу для
              работы над любыми открытыми проектами...
            </p>
            <a href="#" class="page-user-card__link">
              Смотреть проект
            </a>
          </div>
        </article>
        <!-- card -->
        <article class="page-user-card">
          <img src="../../img/page-user/page-user-bg3.jpg" alt="" class="page-user-card__img">
          <div class="page-user-card__content-wrap">
            <h3 class="page-user-card__title">
              Мы хотим вывести идеи Open Source на качественно
            </h3>
            <p class="page-user-card__text">
              Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн платформу для
              работы над любыми открытыми проектами...
            </p>
            <a href="#" class="page-user-card__link">
              Смотреть проект
            </a>
          </div>
        </article>
        <!-- card -->
        <article class="page-user-card">
          <img src="../../img/page-user/page-user-bg4.jpg" alt="" class="page-user-card__img">
          <div class="page-user-card__content-wrap">
            <h3 class="page-user-card__title">
              Мы хотим вывести идеи Open Source на качественно
            </h3>
            <p class="page-user-card__text">
              Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн платформу для
              работы над любыми открытыми проектами...
            </p>
            <a href="#" class="page-user-card__link">
              Смотреть проект
            </a>
          </div>
        </article>
        <!-- card -->
        <article class="page-user-card">
          <img src="../../img/page-user/page-user-bg5.jpg" alt="" class="page-user-card__img">
          <div class="page-user-card__content-wrap">
            <h3 class="page-user-card__title">
              Мы хотим вывести идеи Open Source на качественно
            </h3>
            <p class="page-user-card__text">
              Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн платформу для
              работы над любыми открытыми проектами...
            </p>
            <a href="#" class="page-user-card__link">
              Смотреть проект
            </a>
          </div>
        </article>
        <!-- card -->
        <article class="page-user-card">
          <img src="../../img/page-user/page-user-bg6.jpg" alt="" class="page-user-card__img">
          <div class="page-user-card__content-wrap">
            <h3 class="page-user-card__title">
              Мы хотим вывести идеи Open Source на качественно
            </h3>
            <p class="page-user-card__text">
              Мы хотим вывести идеи Open Source на качественно новый уровень, создав фонд и удобную онлайн платформу для
              работы над любыми открытыми проектами...
            </p>
            <a href="#" class="page-user-card__link">
              Смотреть проект
            </a>
          </div>
        </article>
      </div>
    </div>
  </div>
</div>

<%= _.template(require('./../includes/footer.html').default)(data) %>